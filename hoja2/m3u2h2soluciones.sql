﻿-- (1) CONSULTA 1 y 2 (modificada para ordenarlo por precio)

CREATE OR REPLACE VIEW CONSULTA1 AS
SELECT 
  `CÓDIGO ARTÍCULO` AS `CÓDIGO ARTÍCULO`,
  `SECCIÓN` AS `SECCIÓN`,
  `PRECIO` AS `PRECIO`
FROM productos
WHERE PRECIO>100
ORDER BY PRECIO DESC;

-- (3) Vista consulta2

CREATE OR REPLACE VIEW consulta2 AS
SELECT 
  CONSULTA1.`CÓDIGO ARTÍCULO` AS `CÓDIGO ARTÍCULO`,
  CONSULTA1.`SECCIÓN` AS `SECCIÓN`,
  CONSULTA1.`PRECIO` AS `PRECIO`
FROM CONSULTA1
  WHERE CONSULTA1.`SECCIÓN`='deportes';

-- (5) Insertar un registro mediante la vista consulta 1
INSERT INTO CONSULTA1 VALUES('AR90','Novedades',5);

-- (7) Después de activar el check local en consulta 1, insertar un registro
INSERT INTO CONSULTA1 VALUES('AR91','Novedades',5);

-- (8) Insertar un registro en productos a través de la consulta2

INSERT INTO consulta2 VALUES('AR92','Novedades',5);

-- (9) ACTIVAR CHECK y hacer otra inserción

INSERT INTO consulta2 VALUES('AR93','Deportes',5);

-- (11) cambiado el check de consulta2 a cascada

INSERT INTO consulta2 VALUES('AR94','DEPORTES',5);